import express from 'express';
import {config} from './config/config';

const app = express();

const index = require('./pages/index/controller');
const order_list = require('./pages/order-list/controller');

const api = require('./api/controller');
const error = require('./pages/error/controller');

app.use(express.static(config.static));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use('/', index);
app.use('/order-list', order_list);
app.use('/api/*', api);
app.use('/*', error);


app.listen(config.port, () => console.log(`Running on port ${config.port}`));