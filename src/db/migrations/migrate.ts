const fs = require('fs');
const path = require('path');
const migrations = path.join(__dirname, '../../../migrations/');
var db = require("../psql/connect");
import {migrate} from './fn/migrate';

var files = fs.readdir(migrations, 'utf8', (err, files) => {
    if(err) throw err;
    files.forEach(f => {
        var file = fs.readFileSync(migrations + f, 'utf8');
        migrate(db, file);
    })
    console.log('Tables have been created:', files.join(" "));
})