const Pool = require('pg').Pool;
import {db_settings} from '../../psql/db-settings';
import {migrate} from '../fn/migrate';

var connect = {...db_settings};
var create = `CREATE DATABASE ${connect["database"]}`;
delete connect["database"];

const db = new Pool(connect);

migrate(db, create);