const Pool = require('pg').Pool;
import {db_settings} from '../../psql/db-settings';
import {migrate} from '../fn/migrate';

var connect = {...db_settings};
var drop = `DROP DATABASE IF EXISTS ${connect["database"]}`;
delete connect["database"];

const db = new Pool(connect);

migrate(db, drop);