const Pool = require('pg').Pool;
import {db_settings} from './db-settings';

const psql = new Pool(db_settings);

module.exports = psql;