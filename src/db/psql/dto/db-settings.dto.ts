export interface Settings {
  user:string,
  password:string,
  host:string,
  port:number,
  database:string
}