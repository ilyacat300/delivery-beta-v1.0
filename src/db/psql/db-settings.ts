import {Settings} from './dto/db-settings.dto';

export const db_settings: Settings = {
  user:'postgres',
  password:'password',
  host:'localhost',
  port:5432,
  database:'delivery'
}