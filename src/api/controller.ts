import express, { Request, Response } from 'express';
const router = express.Router();
import {api_controller} from './api-controller/controller';


router.get('/', function(req: Request, res: Response) {
    var api = req.originalUrl.split("/")[2];
    res.send(api_controller(api, 'get request'));
});

router.post('/', function(req: Request, res: Response) {
	var api = req.originalUrl.split("/")[2];
	res.send(api_controller(api, req.body));
});

module.exports = router;