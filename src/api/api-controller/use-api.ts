import {get_order} from './methods/get-order.fn';
import {new_worker} from './methods/new-worker.fn';

export const use_api = {
    "get-order": get_order,
    "new-worker": new_worker
}