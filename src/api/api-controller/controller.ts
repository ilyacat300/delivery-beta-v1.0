import {use_api} from './use-api';

export function api_controller(api, data) {
    switch(api) {
        case "get-order": return use_api[api](data); break;
        case "new-worker": return use_api[api](data); break;
        default: console.log('default switch');
    }
    
}