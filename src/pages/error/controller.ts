import express, { Request, Response } from 'express';
const router = express.Router();
const fs = require('fs');


router.get('/', function(req: Request, res: Response) {
	var file = fs.readFileSync(`pages/error.html`, 'utf8');
	res.send(file);
});


router.post('/', function(req: Request, res: Response) {
	res.send('404');
});

module.exports = router;