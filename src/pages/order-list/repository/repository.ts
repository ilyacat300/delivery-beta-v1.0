var sql = require('../../../db/psql/connect');

class RouteRepository {

    getOrderList = async function() {
        var query = 'SELECT * FROM orders';
        var res = await new Promise(function(resolve, reject) {
            sql.query(query, (err, result) => {
                if(!err) resolve(JSON.stringify(result.rows));
                else console.log('psql err', err);
            });
        })
        return res;
    }
    
}
export var routeRepository = new RouteRepository();