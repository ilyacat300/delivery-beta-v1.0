import express, { Request, Response } from 'express';
const router = express.Router();
const fs = require('fs');
import {routeRepository} from './repository/repository';


router.get('/', function(req: Request, res: Response) {
	var file = fs.readFileSync(`pages/order-list.html`, 'utf8');
	res.send(file);
});


router.post('/', async function(req: Request, res: Response) {
	var result = await routeRepository.getOrderList();
	res.send(result);
});

module.exports = router;