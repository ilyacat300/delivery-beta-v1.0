var sql = require('../../../db/psql/connect');
import {data_dto} from './dto/data.dto';

var data: data_dto = {
    "name": "",
    "adress": "",
    "city": "",
    "phone": "",
    "coordinates": "",
    "order_type": "",
    "comment": ""
}

class RouteRepository {

    addOrder = async function(reqBody) {
        var model = this.model(data, reqBody);
        var query = `INSERT INTO orders ("name", "adress", "city", "phone", "coordinates", "order_type", "comment") 
	    VALUES ($1, $2, $3, $4, $5, $6, $7 )`;
        var result = await new Promise((resolve, rej) => {
            sql.query(query, model, (err, res) => {
                if(!err) resolve("Ваша заявка принята!");
                else resolve('Попробуйте ещё раз');
             })
        })
        return result;
    }

    model = function(data, reqBody) {
        var arr = [];
        for(let i in data) {
            reqBody[i] == "undefined" ? data[i] = "" : data[i] = reqBody[i]; // Если ключа не существует, записываем пустой строкой в БД
            if(i == 'coordinates') { // Координаты это массив из 2 float чисел при успешном наличии
               typeof(data[i]) == 'string' ? arr.push(data[i]) : arr.push(data[i].join(", ")); // БД ожидает строку без скобок
            } else {
                arr.push(data[i]);
            }
        }
        return arr;
    }
    
}

export var routeRepository = new RouteRepository();