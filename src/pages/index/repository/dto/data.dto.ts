export interface data_dto {
    name: string,
    adress: string,
    city: string,
    coordinates: string,
    phone: string,
    order_type: string,
    comment: string
  }