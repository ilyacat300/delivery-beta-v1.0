import {Config} from './dto/config.dto';

const path = require('path');
const root = path.join(__dirname, '../../');
const pages = path.join(__dirname, '../../pages/');
const _static = path.join(__dirname, '../../static/');
const _db = 'psql';
const _redirect = 'http://localhost:5000/';
// ../../ - т.к. этот файл config.ts находится в папке config, выходим из двух папок config -> dist -> root

export const config: Config = {
	port:5000,
	root:root,
	pages:pages,
	static:_static,
	db: _db,
	redirect: _redirect
}