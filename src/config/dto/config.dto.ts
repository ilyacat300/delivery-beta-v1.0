export interface Config {
	port:number;
	root:string;
	pages:string;
	static:string;
	db:string;
	redirect:string;
}