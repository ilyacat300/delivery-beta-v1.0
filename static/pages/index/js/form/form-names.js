export let form_names = {}
export let form_coords = {"coords": ""}

function names_reload() {
	form_names = {
		"name": document.querySelector(".form-name").value,
		"adress": document.querySelector(".form-adress").value || "",
		"city": document.querySelector(".form-city").value || "",
		get full_adress() { return `${this.city} ${this.adress}` },
		"phone": document.querySelector(".form-phone").value || "",
		"order_type": document.querySelector(".form-order").value,
		"comment": document.querySelector(".form-comment").value,
		"coordinates": form_coords.coords
	}
	console.log(form_names)
}
names_reload()

let form = document.querySelector('.form')
form.addEventListener('change', names_reload)