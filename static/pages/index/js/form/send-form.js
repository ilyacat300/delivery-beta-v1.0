export async function send_form(data) {

	if(data.coordinates.length != 2) document.querySelector(".message-for-user").innerText = 'Ваш адрес был указан не верно!'
	else {
		let data = await fetch('http://localhost:5000', { method: 'POST', body: JSON.stringify(data) }).then(item => item.text())
		document.querySelector(".message-for-user").innerText = data
	}
}