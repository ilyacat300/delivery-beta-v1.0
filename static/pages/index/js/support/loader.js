let loader_block = document.querySelector(".loader")
let flag = false

export function loader(state) {

    state ? loading() : loaded()

    function loading() {
        loader_block.style.display = 'flex'
        setTimeout(function() {
            flag = true
            loaded()
        }, 1200)
    }

    function loaded() {
        if(state && flag) {
            loader_block.style.display = 'none'
            flag = false
        }
    }
}