let circles = document.querySelectorAll(".Index .header .circle");
let slides = document.querySelectorAll(".Index .slider .slide");
let btn_box = document.querySelectorAll(".Index .footer .box");

function header(state) {
    circles.forEach(e => e.classList.remove("circle-full"))
    for(let i = 0; i <= state; ++i) circles[i].classList.add("circle-full")
    if(state == 4) document.querySelector(".Index .header .box").style.display = "none"
}

function slider(state) {
    slides.forEach(e => e.style.display = "none")
    slides[state].style.display = "flex"
}

function footer(state) {
    btn_box.forEach(e => e.style.display = "none")
    btn_box[state].style.display = "flex"
}

export function html_support(state) {
    header(state)
    slider(state)
    footer(state)
}