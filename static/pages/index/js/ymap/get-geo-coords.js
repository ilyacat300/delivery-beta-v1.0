import {key} from "../config.js";
import {form_names, form_coords} from "../form/form-names.js";

// adress: string; текстовая форма адреса
export async function getGeoCoords(adress) {

	let data = await fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${key}&format=json&geocode=${adress}`, { method: "GET" }).then(item => item.text())
	data = JSON.parse(data).response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos
	form_coords.coords = data.split(" ").reverse()
	form_names["coordinates"] = form_coords.coords

	return form_coords.coords
}