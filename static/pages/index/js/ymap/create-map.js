import {form_names} from "../form/form-names.js"
import {loader} from "../support/loader.js"
import {getGeoCoords} from "./get-geo-coords.js"

let myMap;

export async function create_ymap() {

    await getGeoCoords(form_names["full_adress"])
    let coords = form_names["coordinates"] // Массив из двух значений

	!myMap ? myMap.destroy() : document.querySelector('#map').style.display = 'block'
    myMap = new ymaps.Map('map', {
        center: coords,
        zoom: 17
    }, {
        searchControlProvider: 'yandex#search'
    })

    let myPlacemarkWithContent = new ymaps.Placemark(coords, {
            hintContent: 'Ваш адресс',
            balloonContent: 'А эта — новогодняя',
            iconContent: ''
        }, {
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'pages/index/img/map/point-tw.png',
            iconImageSize: [48, 48],
            iconImageOffset: [0, -50]
        }
	)

    myMap.geoObjects.add(myPlacemarkWithContent) // Создание точки на карте
    ymaps.ready(function() { loader(false) }) // Скрытие лоадера после загрузки карты
}