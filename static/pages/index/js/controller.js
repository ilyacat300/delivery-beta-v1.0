import {main} from './main/main.js';

let app = document.querySelector('body');

app.addEventListener('click', function(e) {
    if(e.target.hasAttribute('data-app')) main.call(e.target)
})

console.log('app has been started');