import {step_0} from "./all-steps/step-0.js"
import {step_1} from "./all-steps/step-1.js"
import {step_2} from "./all-steps/step-2.js"
import {step_3} from "./all-steps/step-3.js"
import {step_4} from "./all-steps/step-4.js"
import {end} from "./all-steps/end.js"

export const steps = {
    "0": step_0,
    "1": step_1,
    "2": step_2,
    "3": step_3,
    "4": step_4,
    "end": end
}