import {send_form} from "../../../form/send-form.js"
import {form_names} from "../../../form/form-names.js"
import {loader} from "../../../support/loader.js"

export function step_4() {
    loader(true)
    send_form(form_names) // Отправляем данные на сервер
}