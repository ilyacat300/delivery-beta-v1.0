export function step_3() {}

let select = document.querySelector(".form-order")
let images = document.querySelectorAll(".Index .step-3 img")

// Смена изображений при выборе типа заказа в select
select.addEventListener('change', function(e) {
    images.forEach(e =>  e.style.display = "none")
    images[this.selectedIndex].style.display = "block"
})