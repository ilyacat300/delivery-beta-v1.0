import {create_ymap} from "../../../ymap/create-map.js"
import {loader} from "../../../support/loader.js"

export async function step_1() {
    loader(true) // Запускаем лоадер
    create_ymap() // Создаем карту
}