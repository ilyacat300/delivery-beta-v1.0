import {html_support} from "../support/html-support.js"
import {steps} from "./steps/controller.js"

let state = 0

export function main() {
    var value = this.getAttribute('data-app')
    value == 'next' ? state++ : state--
    
    try {html_support(state)} catch(err) {console.log(err)} // try на случай срабатывания с последним html элементом, которого нет

    switch(state){
        case 0: steps[state](); break; // Пустая функция на случай при возврате на стартовую страницу через кнопку "Назад"

        case 1: steps[state](); break // Сохранения контактных данных и создание по ним карты
        case 2: steps[state](); break // Пустая функция
        case 3: steps[state](); break // Пустая функция
        case 4: steps[state](); break // Отправка данных на сервер

        case 5: steps["end"](); break // Завершение работы и перезагрузка страницы
    }
}