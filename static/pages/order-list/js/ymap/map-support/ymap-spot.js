export function map_create_spot(data, counter) {
    return new ymaps.Placemark(data.coordinates.toString().split(","), {
        hintContent: data.name,
        balloonContent: `${data.name} - ${data.phone}`,
        iconContent: counter
    }, {
        iconLayout: 'default#imageWithContent',
        iconImageHref: 'pages/order-list/img/point-tw.png',
        iconImageSize: [48, 48],
        iconImageOffset: [0, -50]
    }
    )
}