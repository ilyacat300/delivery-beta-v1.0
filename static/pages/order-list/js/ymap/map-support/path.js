import {range} from './range.js';

export function path(arr, point) {
    let result = []

    function sort(arr, point) {
        if(arr.length) {
            let index, min_range = 1000000000
   
            for(let i = 0; i < arr.length; ++i) {
                let dist = range(point, arr[i].coordinates.split(", "))
                if(dist < min_range) {
                    min_range = dist
                    index = i
                }
            }
            result.push(arr[index])
            arr.splice(index, 1)
            sort(arr, result.slice(-1).coordinates.split(", "))
        }
    }
    
    sort(arr, point)

    return result
}