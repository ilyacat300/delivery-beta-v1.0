import {loader} from '../loader/loading.js'
import {path} from './map-support/path.js'
import {map_create_spot} from './map-support/ymap-spot.js'
import {table_create_row} from '../table/create-row.js'
import {start_point} from './map-support/start-point.js'

let myMap

export async function create_ymap(data) {
    let arr = path(data, start_point.coordinates);
    table_create_row(arr);
    arr.unshift(start_point);

	!myMap ? myMap.destroy() : document.querySelector('#map').style.display = 'block'
    myMap = new ymaps.Map('map', {
        center: arr[0].coordinates,
        zoom: 17
    }, {
        searchControlProvider: 'yandex#search'
    })

    for(let i = 0; i < arr.length; ++i) myMap.geoObjects.add(map_create_spot(arr[i], i))

    loader(false)
}