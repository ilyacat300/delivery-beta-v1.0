import {create_ymap} from '../ymap/create-map.js';

export async function get_order_list(data = {}) {
	let request = await fetch('http://localhost:5000/order-list', { method: 'POST', body: JSON.stringify(data) }).then(item => item.text())
	request = JSON.parse(request)
	create_ymap(request)
}