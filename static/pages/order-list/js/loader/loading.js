const loader_block = document.querySelector(".loader");

export function loader(data) {
    data ? loader_block.style.display = "flex" : loader_block.style.display = "none"
}