const table = document.querySelector(".table");

export function table_create_row(data) {
    data.forEach(e => table_render(e))
}

function table_render(data) {
    let html = `<tr>
        <td>${data.id}</td>
        <td>${data.name}</td>
        <td>${data.adress}</td>
        <td>${data.phone}</td>
    </tr>`

    table.innerHTML += html
}