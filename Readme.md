# Delivery

## Запуск проекта 

> `Внимание`, если у вас `в PostgreSQL` есть рабочая `БД` с именем `delivery` - она `удалится`!  
1. `npm i` - установить пакеты  
2. `npm run def` - скомпилировать и запустить приложение   

### Команды  
1. `npm run go` - скомпилирует содержимое папки src и запустит сервер без миграций  
2. `npm run migrate` - произведет миграцию в БД из папки migrates  
3. `npm run start` - запуск сервера  

### index.html  
1. Форма заказа
2. Отправка данных на сервер
3. Запись в БД и ответ клиенту

### order-list.html
1. Получение списка заказов

### /api/*
1. Возможность добавлять api методы  
2. Тестовые запросы `/api/get-order` и `/api/get-worker`

### Error 404
Страница ошибки 


