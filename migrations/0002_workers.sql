create TABLE workers (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    profession VARCHAR(255)
)