create TABLE orders (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    adress VARCHAR(255),
    city VARCHAR(255),
    coordinates VARCHAR(255),
    phone VARCHAR(255),
    order_type VARCHAR(255),
    comment VARCHAR(255)
)